FROM python:3.12.0a2-alpine3.16
RUN apk --no-cache add git && git clone https://github.com/tornadoweb/tornado.git
WORKDIR /tornado
COPY tornado.py /tornado/tornado.py
CMD ["python", "tornado.py"]
EXPOSE 8888/tcp
